from datetime import datetime
from typing import Any, Callable, Dict, List
from uuid import uuid4
import calendar
import os
import sys

from bs4 import BeautifulSoup as Soup
import requests


def spawn(func: Callable[[], None]):
    """See Stevens' "Advanced Programming in the UNIX Environment" for details
    (ISBN 0201563177)
    Credit: https://stackoverflow.com/a/6011298/374110

    Takes a callable which will be called in the fork.
    """
    try:
        pid = os.fork()
        if pid > 0:
            # parent process, return and keep running
            return
    except OSError as exc:
        print('Fork #1 failed: {} ({})'.format(exc.errno, exc.strerror))
        sys.exit(1)

    os.setsid()

    # do second fork
    try:
        pid = os.fork()
        if pid > 0:
            # exit from second parent
            sys.exit(0)
    except OSError as exc:
        print('Fork #2 failed: {} ({})'.format(exc.errno, exc.strerror))
        sys.exit(1)

    func()

    os._exit(os.EX_OK)


def request_raise(session: requests.Session, *args: List[Any],
                  **kwargs: Dict[str, Any]) -> requests.Response:
    meth = getattr(session, kwargs.pop('method', 'get').lower())
    req = meth(*args, **kwargs)
    req.raise_for_status()
    return req


def soup_request(session: requests.Session, *args: List[Any],
                 **kwargs: Dict[str, Any]) -> Soup:
    parser = kwargs.pop('parser', 'lxml')
    return Soup(request_raise(session, *args, **kwargs).text, parser)


def s_uuid4() -> str:
    return str(uuid4())


def time_ms(ref: datetime) -> int:
    return calendar.timegm(ref.timetuple()) * 1000


def add_cookies(session: requests.Session, domain: str,
                **kwargs: Dict[str, str]) -> None:
    for name, value in kwargs.items():
        session.cookies.set(name, value, domain=domain)
