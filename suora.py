"""Main module."""
from datetime import datetime
from shlex import quote
from typing import Any, Callable, Dict, List
import argparse

import os
import signal
import subprocess as sp
import sys

import requests

from spectrum import spectrum_login
from strategies import BaseExtractor, FoxNewsExtractor
from util import (add_cookies, request_raise, s_uuid4, soup_request, spawn,
                  time_ms)
import config

strats = dict(foxnews=FoxNewsExtractor())


def main() -> int:
    parser = argparse.ArgumentParser()

    parser.add_argument('channel')
    parser.add_argument('username')
    parser.add_argument('password')
    parser.add_argument(
        'action', choices=('record', 'watch', 'print'), default='print')

    args = parser.parse_args()
    session = requests.Session()
    try:
        extractor: BaseExtractor = strats[args.channel]
    except KeyError:
        print('Channel "{}" is invalid'.format(args.channel), file=sys.stderr)
        return 1

    extractor.preflight(session)
    spectrum_login(session, args.username, args.password, extractor.urn)

    full_uri = extractor.get_streaming_uri(session)
    qf = quote(full_uri)
    ts = str(time_ms(datetime.now()))
    fn = '{}-{}.mkv'.format(args.channel, ts)

    if args.action == 'print':
        title_arg = quote('--title={}'.format(extractor.title))

        print('mpv {} {}\n'.format(title_arg, qf))
        print('ffmpeg -i {} -codec copy -map 0:1 -map 0:2 {}'.format(
            qf, quote(fn)))
    elif args.action == 'watch':
        cmd = ['mpv', '--title={}'.format(extractor.title), full_uri]
        spawn(lambda: sp.run(cmd, stderr=sp.PIPE, stdout=sp.PIPE))
    else:
        logfile = './{}-{}.log'.format(args.channel, ts)
        with open(logfile, 'w+') as f:
            cmd = [
                'ffmpeg', '-nostats', '-hide_banner', '-v', 'warning', '-i',
                full_uri, '-codec', 'copy', '-map', '0:1', '-map', '0:2', fn
            ]
            p = sp.Popen(cmd, stderr=f, stdout=f, start_new_session=True)
            try:
                print('ffmpeg is running. ^C (once!) to exit', file=sys.stderr)
                p.wait()
            except KeyboardInterrupt:
                print('Wait for the file to finish writing.', file=sys.stderr)
                os.killpg(os.getpgid(p.pid), signal.SIGINT)

    return 0


if __name__ == '__main__':
    sys.exit(main())
