import requests


class BaseExtractor:
    def preflight(self, session: requests.Session) -> None:
        pass

    def get_streaming_uri(self, session: requests.Session) -> str:
        raise NotImplementedError()
