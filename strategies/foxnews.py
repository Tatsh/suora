import requests

from config import securetve_host, securetve_rest_path
from util import request_raise

__all__ = ['FoxNewsExtractor']


class FoxNewsExtractor:
    guid = '5614615980001'
    urn = 'urn:foxnews:com:sp:site:2'
    resource_access_uri = (
        'https://{}/{}/{}/identity/resourceAccess/FoxBusiness').format(
            securetve_host, securetve_rest_path, urn)
    watch_uri = 'https://video.foxnews.com/v/{}/?#sp=watch-live'.format(guid)
    title = 'Fox News'

    def preflight(self, session: requests.Session) -> None:
        request_raise(session, self.watch_uri)

    def get_streaming_uri(self, session: requests.Session) -> str:
        params = dict(format='json', responsefield='aisresponse')
        r = request_raise(session, self.resource_access_uri, params=params)
        sec_token = r.json()['security_token']

        query = dict(template='fox')
        feed_uri = 'https://video.foxnews.com/v/feed/video/{}.js'.format(
            self.guid)

        r = request_raise(session, feed_uri, params=query)
        m3u8_uri = None

        for content in r.json(
        )['channel']['item']['media-group']['media-content']:
            attr = content['@attributes']
            if attr['url'].endswith('m3u8'):
                m3u8_uri = attr['url']
                break

        if not m3u8_uri:
            raise ValueError('Could not get m3u8 URI')

        return '{}?hdnea={}'.format(m3u8_uri, sec_token)
