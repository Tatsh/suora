from datetime import datetime

import requests

from util import add_cookies, request_raise, soup_request, s_uuid4, time_ms
import config


def spectrum_login(session: requests.Session, username: str, password: str,
                   urn: str):
    query = dict(
        responsemethod='redirect', format='jsonp', responsetarget='live_fn')
    soup = soup_request(
        session,
        config.spectrum['login_start_uri'] % {'urn': urn},
        params=query)
    form = soup.select('[action="/openam/UI/Login"]')[0]
    data = dict(IDToken1=username, IDToken2=password)
    for hidden in form.select('input[type="hidden"]'):
        data[hidden['name']] = hidden['value']
    data['rememberMe'] = 'on'
    data['referer'] = 'null'

    guid = s_uuid4()
    d = datetime.now()
    ts = str(time_ms(d))

    spectrum_cookies = dict(
        device_type='unknown',
        provider='undefined/{}'.format(config.urns['akamai']),
        requester=config.urns['akamai'],
        visitId=guid,
        transactionId='{}|{}'.format(guid, ts),
        loginStartTime=ts,
        user=username)
    add_cookies(session, config.spectrum['idp_host'], **spectrum_cookies)

    soup = soup_request(
        session,
        config.spectrum['login_uri'],
        data=data,
        allow_redirects=False,
        method='POST')
    form = soup.select('form')[0]
    post_uri = form['action']
    data = dict(SAMLResponse=form.select('[name="SAMLResponse"]')[0]['value'])
    request_raise(session, post_uri, data=data, method='POST')
