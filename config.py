guids = dict(fox_business='5614626175001', fox_news='5614615980001')

securetve_host = 'idp.securetve.com'
securetve_rest_path = '/rest/1.0'

urns = dict(
    akamai='urn:akamai:com:ais:sp:1', charter='urn:charter:com:idp:prod')

spectrum = dict(
    idp_host='idp.aws.spectrum.net',
    login_start_uri='http://{}{}/%(urn)s/init/{}'.format(
        securetve_host, securetve_rest_path, urns['charter']),
    login_uri='https://idp.aws.spectrum.net/openam/UI/Login')
